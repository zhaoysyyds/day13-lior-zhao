import request from "./request";
export const loadTodos = () => {
  return request.get("");
};

export const updateTodo = (id, todo) => {
  return request.put(`/${id}`, todo);
};

export const deleteTodo = (id) => {
  return request.delete(`/${id}`);
};

export const addTodo = (todo) => {
  return request.post(``, todo);
};

export const findTodoById = (id) => {
    return request.get(`/${id}`);
};

export const loadDoneTodos = (done) => {
  return request.get(`?done=${done}`);
};
