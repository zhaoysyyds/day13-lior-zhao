/*
 * @Autor: Haiting Zhao
 * @Date: 2023-07-24 19:31:39
 * @LastEditors: Haiting Zhao
 * @LastEditTime: 2023-07-24 19:55:20
 * @Description: file content
 * @FilePath: \todo-list\src\App.js
 */
import "./App.css";
import * as React from "react";
import { RouterProvider } from "react-router-dom";
import { router } from "./router";

function App() {
  return (
    <div className="App">
      <RouterProvider router={router} />
    </div>
  );
}

export default App;
