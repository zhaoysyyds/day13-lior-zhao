import { useDispatch } from "react-redux";
import { initTodos } from "../components/todoSlice";
import { loadTodos } from "../apis/todo";
import { useRef } from "react";
const useTodo = () => {
  const dispatch = useDispatch();

  const reloadTodos = () => {
    loadTodos().then((response) => {
      dispatch(initTodos(response.data));
    });
  };

  return useRef({ reloadTodos }).current;
};
export default useTodo;
