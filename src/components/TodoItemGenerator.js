import { useState } from "react";
import { addTodo } from "../apis/todo";
import useTodo from "./../hooks/useTodo";
import { Button, Input } from "antd";
const TodoItemGenerator = (props) => {
  const [content, setContent] = useState("");
  const { reloadTodos } = useTodo();
  const handleChangeInput = (e) => {
    setContent(e.target.value);
  };

  const handleChange = async () => {
    if (content.trim() === "") return;
    const todo = {};
    todo.text = content;

    await addTodo(todo);
    reloadTodos();

    setContent("");
  };
  const handleKeyUp = (event) => {
    if (event.keyCode === 13) {
      handleChange();
    }
  };
  return (
    <div className="generator">
      <Input
        placeholder="input message"
        style={{ width: 220 }}
        onKeyUp={handleKeyUp}
        type="text"
        value={content}
        onChange={handleChangeInput}
      ></Input>
      &nbsp;&nbsp;&nbsp;&nbsp;
      <Button type="primary" onClick={handleChange}>
        add
      </Button>
    </div>
  );
};

export default TodoItemGenerator;
