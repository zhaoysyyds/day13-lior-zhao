import "../style/todoItem.css";
import { useNavigate } from "react-router-dom";
import { deleteTodo, updateTodo } from "./../apis/todo";
import useTodo from "../hooks/useTodo";
import { Button, Modal } from "antd";
import { useState } from "react";
const TodoItem = (props) => {
  const { todo } = props;
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [message, setMessage] = useState(todo.text);
  const navigate = useNavigate();
  const { reloadTodos } = useTodo();
  const changeMessage = (e) => {
    setMessage(e.target.value);
  };
  const changeDone = async () => {
    if (props.change) {
      await updateTodo(todo.id, { done: !todo.done });

      reloadTodos();
    } else {
      navigate(`/todo/${todo.id}`);
    }
  };
  const toDelete = async (event) => {
    event.stopPropagation();
    if (!props.change) {
      return;
    }
    await deleteTodo(todo.id);
    reloadTodos();
  };

  const showModal = () => {
    if (!props.change) {
      return;
    }
    setMessage(todo.text);
    setIsModalOpen(true);
  };
  const handleOk = async () => {
    if (message.trim() === "") {
      alert("文本不能为空");
      return;
    }

    await updateTodo(todo.id, { text: message.trim(), done:todo.done });

    reloadTodos();
    setIsModalOpen(false);
  };
  const handleCancel = () => {
    setIsModalOpen(false);
  };

  return (
    <div className="todo-item">
      <div
        className={props.todo.done ? "text-done" : "text"}
        onClick={changeDone}
      >
        {props.todo.text}
      </div>
      <div className="icon">
        <Button onClick={showModal} shape="circle" size="small" type="primary">
          E
        </Button>
        &nbsp;
        <svg
          onClick={toDelete}
          xmlns="http://www.w3.org/2000/svg"
          width="24"
          height="24"
          fill="currentColor"
          className="bi bi-x-circle"
          viewBox="0 0 16 16"
        >
          <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z" />
          <path d="M4.646 4.646a.5.5 0 0 1 .708 0L8 7.293l2.646-2.647a.5.5 0 0 1 .708.708L8.707 8l2.647 2.646a.5.5 0 0 1-.708.708L8 8.707l-2.646 2.647a.5.5 0 0 1-.708-.708L7.293 8 4.646 5.354a.5.5 0 0 1 0-.708z" />
        </svg>
      </div>
      <Modal
        title="Edit Message"
        open={isModalOpen}
        onOk={handleOk}
        onCancel={handleCancel}
      >
        <input value={message} onChange={changeMessage}></input>
      </Modal>
    </div>
  );
};

export default TodoItem;
