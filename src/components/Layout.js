import { NavLink, Outlet } from "react-router-dom";

const Layout = () => {
  return (
    <div>
      <div className="header">
        <NavLink to="/">todoList</NavLink>
        <NavLink to="/doneList">doneList</NavLink>
        <NavLink to="/about">about</NavLink>
      </div>
      <Outlet></Outlet>
    </div>
  );
};

export default Layout;
