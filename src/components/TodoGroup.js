import TodoItem from "./TodoItem";

const TodoGroup = (props) => {
    return (
      <div>
        {props.todoList.map((todo, index) => (
          <TodoItem key={index} todo={todo} change={props.change}></TodoItem>
        ))}
      </div>
    );
};

export default TodoGroup;
