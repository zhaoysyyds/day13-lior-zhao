import TodoGroup from "./TodoGroup";
import TodoItemGenerator from "./TodoItemGenerator";
import "./../style/todolist.css";
import { useSelector } from "react-redux";
import { useEffect } from "react";
import useTodo from "./../hooks/useTodo";

const TodoList = () => {
  const todoList = useSelector((state) => state.todoList.todoList);

  const { reloadTodos } = useTodo();

  useEffect(() => {
    reloadTodos();
  }, [reloadTodos]);

  return (
    <>
      <div>
        <h1 className="todo-list">Todo List</h1>
        <TodoGroup todoList={todoList} change={true}></TodoGroup>
        <TodoItemGenerator></TodoItemGenerator>
      </div>
    </>
  );
};

export default TodoList;
