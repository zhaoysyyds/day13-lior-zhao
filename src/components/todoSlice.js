import { createSlice } from "@reduxjs/toolkit";

export const todoSlice = createSlice({
  name: "todo",
  initialState: {
    todoList: [],
  },
  reducers: {
    initTodos: (state, action) => {
      state.todoList = action.payload;
    },
  },
});
export const { onAdd, initTodos } = todoSlice.actions;
export default todoSlice.reducer;
