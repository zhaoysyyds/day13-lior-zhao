import TodoGroup from "../../components/TodoGroup";
import { useEffect, useState } from "react";
import { loadDoneTodos } from "../../apis/todo";

const DoneList = () => {
  const [doneList, setDoneList] = useState([]);

  useEffect(() => {
    loadDoneTodos(true).then((response) => {
      setDoneList(response.data);
    });
  }, []);

  return (
    <>
      <div>
        <h1>done list</h1>
        <TodoGroup todoList={doneList} change={false}></TodoGroup>
      </div>
    </>
  );
};

export default DoneList;
