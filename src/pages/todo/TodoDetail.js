import { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import { findTodoById } from "../../apis/todo";

const TodoDetail = () => {
  const { id } = useParams();
  const [ todo, setTodo ] = useState({});

  console.log(todo);
  const navigate = useNavigate();

  useEffect(() => {
    findTodoById(id).then((response) => {
      setTodo(response.data);
    })
    if (!todo) {
      navigate("/404");
    }
  }, []);

  return (
    <div>
      <h1>Todo Detail</h1>
      {todo && (
        <div>
          <h2>text:{todo.text}</h2>
        </div>
      )}
    </div>
  );
};

export default TodoDetail;
